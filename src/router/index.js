import Vue from 'vue'
import Router from 'vue-router'
 
//组件模块
import List from '../components/list'
import About from '../components/about'

 
Vue.use(Router)
 
export default new Router({
  routes: [
    { path: '/list', name: 'List', component: List },
    { path: '/about',  name: 'Aabout', component: About},
    
  ]
})
