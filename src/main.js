import Vue from 'vue';
import App from './App.vue';
import router from './router';
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import axios from 'axios';

Vue.prototype.$http = axios;
Vue.use(iView);
axios.defaults.baseURL = 'http://192.168.31.117:8088/';//本地调试
new Vue({
  el: '#app',
  render: h => h(App),
  router
})
